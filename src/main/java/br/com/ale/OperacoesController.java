package br.com.ale;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class OperacoesController {
	
	String listarReverso() {
		System.out.println("Listar Reverso");
		return "Teste Metodo";
	}
	
	String imprimirImpares() {
		System.out.println("Imprimir Impares");
		return "Imprimir Impares";
	}
	
	@RequestMapping("/listaReversa")
	public List<String> Op1(@RequestParam(value="lista") String lista ) {
		List<String> items = Arrays.asList(lista.replace("{","").replace("}","").split(","));
		Collections.reverse(items);
		return items;
	}
	
	@RequestMapping("/imprimirImpares")
	public String Op2( ) {
		return imprimirImpares();
	}	
	
}
